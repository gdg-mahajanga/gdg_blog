module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        login: 'url("./src/assets/images/bg-login.jpg")',
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
