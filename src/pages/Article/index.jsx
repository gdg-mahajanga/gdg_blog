import React, { useContext, useEffect, useRef, useState } from "react";
import ReactMarkdown from "react-markdown";
import { MainLayout } from "../../layouts/main";
import gfm from "remark-gfm";
import rehypeRaw from "rehype-raw";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { darcula } from "react-syntax-highlighter/dist/esm/styles/prism";
import moment from "moment";
import { BlogLoader } from "./Loader";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShare } from "@fortawesome/free-solid-svg-icons";
import {
  getBlob,
  getBytes,
  getDownloadURL,
  getStorage,
  getStream,
  ref,
} from "firebase/storage";
import Input, { TextArea } from "../../components/Input";
import { useCollection, useDocument } from "react-firebase-hooks/firestore";
import toast from "react-hot-toast";
import { collection, doc, getFirestore } from "firebase/firestore";
import { firebaseApp } from "../../services/firebase.service";
import { useParams } from "react-router-dom";
import { motion } from "framer-motion";
import { states } from "../../states";
import { useSnapshot } from "valtio";
import { addData } from "../../services/crud.service";
import Avatar from "../../components/Avatar/Avatar";

export const components = {
  code({ node, inline, className, children, ...props }) {
    const match = /language-(\w+)/.exec(className || "");
    return !inline && match ? (
      <SyntaxHighlighter
        style={darcula}
        language={match[1]}
        PreTag="div"
        children={String(children).replace(/\n$/, "")}
        {...props}
      />
    ) : (
      <code className={className} {...props} />
    );
  },
};

export default function ArticlePage(props) {
  const { id } = useParams();
  const [file, setFile] = useState("");
  const [article, setArticle] = useState({});

  const [value, loading, error] = useDocument(
    doc(getFirestore(firebaseApp), "articles", id || props.article),
    {
      snapshotListenOptions: { includeMetadataChanges: true },
    }
  );

  useEffect(() => {
    if (error) {
      toast.error(error);
    }

    if (value) {
      setArticle(value.data());

      setFile(value.data().content);

      const fileUrl = value.data().fileURL;
      const image = value.data().image;

      if (fileUrl) {
        getBlob(ref(getStorage(firebaseApp), value.data().fileURL))
          .then(async (file) => {
            const articleContent = await file.text();
            setFile(articleContent);
          })
          .catch((err) => toast.error(err.message));
      }

      if (image.startsWith("gs")) {
        getDownloadURL(ref(getStorage(firebaseApp), value.data().image))
          .then(async (image) => {
            await setArticle({ ...value.data(), image: image });
          })
          .catch((err) => toast.error(err.message));
      }
    }
  }, [loading]);

  return (
    <MainLayout>
      <motion.div
        key="article.frame"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
      >
        {loading ? (
          <BlogLoader></BlogLoader>
        ) : (
          <BlogLayout article={article} file={file} articleId={id}></BlogLayout>
        )}
      </motion.div>
    </MainLayout>
  );
}

const BlogLayout = ({ article, file, articleId }) => {
  return (
    <div className="px-8 py-8 lg:px-40">
      <img src={article.image} className="my-8 w-full" alt="background"></img>
      {article.categories &&
        article.categories.map((category, index) => {
          return (
            <a
              href="#"
              key={index}
              className="font-normal rounded-sm text-sm px-2 py-1 mr-2 bg-green-100 inline hover:text-blue-800 hover:font-bold"
            >
              #{category}
            </a>
          );
        })}
      <h2 className="text-2xl my-8 font-body font-bold">{article.title}</h2>
      <div className="prose lg:prose-lg xl:prose-xl">
        <MarkdownComponent file={file}></MarkdownComponent>
      </div>
      {article.author && (
        <BlogFooter
          author={article.author}
          date={moment(article.date.toDate()).format("DD MMM YYYY")}
        ></BlogFooter>
      )}

      <LeaveComment articleId={articleId} />
      <ListComments articleId={articleId} />
    </div>
  );
};

export const MarkdownComponent = ({ file }) => {
  return (
    <ReactMarkdown
      rehypePlugins={[rehypeRaw]}
      components={components}
      remarkPlugins={[gfm]}
      children={file}
    />
  );
};

const BlogFooter = ({ author, date }) => {
  return (
    <div className="flex flex-row my-4 items-center border-t-2 py-8">
      {author.avatar ? (
        <img className="rounded-full w-16" src={author.avatar} />
      ) : (
        <span className="font-bold rounded-full text-center   bg-green-600 h-12 w-12 p-2 text-white text-2xl">
          {author.name.charAt(0)}
        </span>
      )}
      <div className="flex flex-col ml-4">
        <h3 className="font-bold">
          {author.name} -{" "}
          {author.title && (
            <span className="italic font-light">{author.title}</span>
          )}
        </h3>
        <h4 className="text-gray-500 text-xs">Published at {date}</h4>
      </div>
    </div>
  );
};

const LeaveComment = ({ articleId }) => {
  const snapshot = useSnapshot(states);

  return (
    <>
      <h2 className="font-bold text-xl my-8">Leave a comment</h2>
      <div className="my-2">
        <Input
          key="name"
          name="commentName"
          placeholder="Your name"
          isRequired={true}
          value={snapshot.input.commentName}
        ></Input>
      </div>

      <div className="mb-2">
        <Input
          key="name"
          name="commentEmail"
          placeholder="Your email"
          isRequired={true}
          value={snapshot.input.commentEmail}
        ></Input>
      </div>
      <TextArea
        name="commentMessage"
        placeholder="Votre message"
        className="w-full"
        isRequired={true}
        value={snapshot.input.commentMessage}
      ></TextArea>

      <button
        onClick={async () => {
          const date = new Date();
          const dateStr =
            ("00" + date.getDate()).slice(-2) +
            "/" +
            ("00" + (date.getMonth() + 1)).slice(-2) +
            "/" +
            date.getFullYear() +
            " " +
            ("00" + date.getHours()).slice(-2) +
            ":" +
            ("00" + date.getMinutes()).slice(-2) +
            ":" +
            ("00" + date.getSeconds()).slice(-2);

          const commentData = {
            article_id: articleId,
            user_name: snapshot.input.commentName,
            user_email: snapshot.input.commentEmail,
            comment: snapshot.input.commentMessage,
            createdAt: dateStr,
          };

          states.input.commentName = "";
          states.input.commentEmail = "";
          states.input.commentMessage = "";

          await addData("comments", commentData);
        }}
        className="border-green-300 border-2 py-2 px-12 shadow-md rounded-full mt-8 hover:bg-green-300 hover:text-white transition-all duration-500"
      >
        <FontAwesomeIcon className="mr-2" icon={faShare}></FontAwesomeIcon> Send
      </button>
    </>
  );
};

const ListComments = ({ articleId }) => {
  const [value, loading, error] = useCollection(
    collection(getFirestore(firebaseApp), "comments"),
    {
      snapshotListenOptions: { includeMetadataChanges: true },
    }
  );

  return (
    <>
      <h2 className="font-bold text-xl my-8">Comments</h2>
      <div>
        {value &&
          value.docs
            .filter((doc) => doc.data().article_id == articleId)
            .map((doc) => {
              return (
                <SingleComment
                  key={doc.id}
                  author={doc.data().user_name}
                  email={doc.data().user_email}
                  comment={doc.data().comment}
                  createdAt={doc.data().createdAt}
                />
              );
            })}
      </div>
    </>
  );
};

const SingleComment = ({ author, email, comment, createdAt }) => {
  return (
    <div className="shadow-lg w-full p-8">
      <div className="flex flex-row justify-start gap-x-4 items-center">
        <Avatar size="md"></Avatar>
        <div>
          <h3>
            By <span className="font-bold">{author}</span> -{" "}
            <span className="italic">{email}</span>
          </h3>
          <h4 className="text-gray-500">{createdAt}</h4>
        </div>
      </div>
      <p className="text-justify pl-20">{comment}</p>
    </div>
  );
};
