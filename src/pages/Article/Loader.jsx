import React from "react";

export const BlogLoader = () => {
  return (
    <div className="px-8 py-8 lg:px-40">
      <div className="box h-80"></div>
      <div className="box h-4 w-2/3 mt-8"></div>
      <div className="box h-4 w-1/6 mt-8"></div>
      <div className="box h-4 mt-8"></div>
      <div className="box h-4 mt-8"></div>
      <div className="box h-4 mt-8"></div>
      <div className="box h-4 mt-8"></div>
      <div className="box h-4 mt-8"></div>
      <div className="box h-4 mt-8"></div>
      <div className="flex flex-row">
        <div className="box h-12 p-2 w-12 rounded-full mt-8"></div>
        <div className="flex flex-col">
          <div className="box h-4 w-full"></div>
          <div className="box h-4 w-full"></div>
        </div>
      </div>
    </div>
  );
};
