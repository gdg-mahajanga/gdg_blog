import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Button } from "../../components/Buttons";
import Input from "../../components/Input";
import Title from "../../components/Title";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { useSnapshot } from "valtio";
import { states } from "../../states";
import { addData } from "../../services/crud.service";
import background from "../../assets/images/register.jpg";
import toast from "react-hot-toast";
import { actions } from "../../actions";
import { motion } from "framer-motion";

const RegisterPage = () => {
  const { input, isLoading, showMessage } = useSnapshot(states);
  const register = () => {
    actions.toggleLoader();
    const auth = getAuth();
    createUserWithEmailAndPassword(auth, input.email, input.password)
      .then((userCredential) => {
        const user = userCredential.user;
        if (user) {
          addData("users", input, user.uid);
          toast.success(`Your account has been created`);
        }
      })
      .catch((error) => {
        toast.error(`${error.code}\n${error.message}`);
      })
      .finally(() => {
        actions.toggleLoader();
      });
  };

  return (
    <motion.div
      key="register.frame"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <div className="flex flex-row-reverse min-h-screen">
        <div
          className="hidden lg:flex-1 bg-cover bg-center blur-xs"
          style={{
            backgroundImage: `url(${background})`,
          }}
        ></div>
        <div className="flex-1 flex flex-col justify-center items-center px-16">
          <Title className="font-bold text-center">Créer ton compte</Title>
          <span className="my-8"></span>
          <Input placeholder="Pseudonyme" name="name" />
          <span className="my-2"></span>
          <Input placeholder="Email" type="email" name="email" />
          <span className="my-2"></span>
          <Input placeholder="Mot de passe" type="password" name="password" />
          <span className="my-2"></span>
          <Input placeholder="Poste" name="title" />
          <span className="my-4"></span>
          <Button
            wide
            loading={isLoading}
            disabled={isLoading}
            onClick={register}
          >
            Créer un compte
          </Button>
          <span className="my-2"></span>
          <Link to="/login">
            <span className="text-blue-600 text-center">
              Vous avez déjà un compte?
            </span>
          </Link>
        </div>
      </div>
    </motion.div>
  );
};

export default RegisterPage;
