import React, { useEffect, useState } from "react";
import { getFirestore, collection, doc, getDoc } from "firebase/firestore";
import { useCollection } from "react-firebase-hooks/firestore";
import Input from "../../components/Input";
import { Button } from "../../components/Buttons";
import Title from "../../components/Title";
import { Link, useNavigate } from "react-router-dom";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { states } from "../../states";
import { useSnapshot } from "valtio";
import { actions } from "../../actions";
import { firebaseApp } from "../../services/firebase.service";
import { motion } from "framer-motion";
import toast from "react-hot-toast";

const LoginPage = (props) => {
  let navigate = useNavigate("");
  const { input, isLoading } = useSnapshot(states);
  const { showMessage } = useSnapshot(states);
  const db = getFirestore(firebaseApp);
  const [value, loading, error] = useCollection(collection(db, "tools"), {
    snapshotListenOptions: { includeMetadataChanges: true },
  });

  useEffect(() => {
    if (error) {
      console.error(error);
    }
  }, [loading]);

  const login = () => {
    const auth = getAuth();
    actions.toggleLoader();
    signInWithEmailAndPassword(auth, input.email, input.password)
      .then(async (userCredential) => {
        const userRef = doc(db, "users", userCredential.user.uid);
        const userSnap = await getDoc(userRef);

        if (userSnap.exists()) {
          localStorage.setItem(
            "user",
            JSON.stringify({ ...userSnap.data(), uid: userCredential.user.uid })
          );
          await setTimeout(() => {
            window.location.reload();
          }, 3000);
          navigate("/");
        }
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        toast.error(errorCode, errorMessage);
      })
      .finally(() => {
        actions.toggleLoader();
      });
  };

  return (
    <motion.div
      key="login.frame"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <div className="flex flex-row min-h-screen">
        <div className="md:flex-1 bg-login bg-cover"></div>
        <div className="md:flex-1 flex flex-col justify-center items-center px-16">
          <Title className="font-bold text-center">
            Vous devez être connecté si vous voulez écrire des articles.{" "}
          </Title>
          <span className="text-md my-2">
            Veuillez entrer vos identifiants.
          </span>
          <span className="my-8"></span>
          <Input placeholder="Email" type="email" name="email" />
          <span className="my-2"></span>
          <Input placeholder="Password" name="password" type="password" />
          <span className="my-2"></span>
          <div className="flex justify-end items-end w-full">
            <Button
              type="text"
              className="text-sm"
              onClick={() => {
                const screenToNavigate = "/reset-password";
                navigate(screenToNavigate);
              }}
            >
              <span className="text-blue-600">Mot de passe oublié?</span>
            </Button>
          </div>
          <span className="my-4"></span>
          <Button wide disabled={isLoading} loading={isLoading} onClick={login}>
            Se connecter
          </Button>
          <span className="my-2"></span>
          <Link to="/register">
            <span className="text-blue-600">Créer un compte</span>
          </Link>
          <Link to="/" className="my-4">
            <span className="text-blue-600">
              Je veux juste lire des articles
            </span>
          </Link>
        </div>
      </div>
    </motion.div>
  );
};

export default LoginPage;
