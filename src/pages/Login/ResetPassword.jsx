import React from "react";
import { Link } from "react-router-dom";
import bgResetPassword from "../../assets/images/bg-reset-password.jpeg";
import forgotPassword from "../../assets/images/forgot-password.svg";
import { Button } from "../../components/Buttons";
import Input from "../../components/Input";
import Title from "../../components/Title";
import { getAuth, sendPasswordResetEmail } from "firebase/auth";
import toast from "react-hot-toast";
import { useSnapshot } from "valtio";
import { states } from "../../states";

const ResetPassword = () => {
  const { input } = useSnapshot(states);

  const auth = getAuth();

  const triggerPasswordReset = () => {
    const email = input["resetPassword"];

    sendPasswordResetEmail(auth, email)
      .then(() => {
        toast.success("Veuillez consulter votre boîte mail");
      })
      .catch((error) => {
        toast.error(error.message);
      });
  };

  return (
    <div className="flex flex-col justify-center md:flex-row md:justify-start h-[75vh] md:h-screen">
      <img
        className="hidden md:flex md:w-1/2 md:h-screen object-cover"
        src={bgResetPassword}
        alt="Reset password"
      />
      <img
        className="flex p-8 md:hidden h-64 object-cover"
        src={forgotPassword}
        alt="Forgot password"
      />
      <div className="px-8 md:px-12 py-3 md:w-1/2 flex flex-col justify-center items-center gap-4">
        <Title type={"h2"} className="text-center">
          Réinitialiser votre mot de passe
        </Title>
        <Input
          type={"email"}
          name="resetPassword"
          placeholder="Enter your email"
          required
        />
        <Button wide onClick={triggerPasswordReset}>
          Réinitialiser
        </Button>
        <Link to="/">
          <span className="text-blue-600">Revenir à l'acceuil</span>
        </Link>
      </div>
    </div>
  );
};

export default ResetPassword;
