import React, { useEffect, useState } from "react";
import { addData } from "../../services/crud.service";
import { MainLayout } from "../../layouts/main";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { collection, onSnapshot, getFirestore } from "firebase/firestore";
import { getStorage, ref, uploadBytes } from "firebase/storage";
import toast from "react-hot-toast";
import Input from "../../components/Input";
import { MarkdownComponent } from "../Article";
import { firebaseApp } from "../../services/firebase.service";
import { useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { Button } from "../../components/Buttons";
import { useNavigate } from "react-router-dom";
import { actions } from "../../actions";
import { useSnapshot } from "valtio";
import { states } from "../../states";

const EditorPage = () => {
  const [articleDescription, setArticleDescription] = useState("");
  const [articleData, setArticleData] = useState("");
  const [author, setAuthor] = useState("");
  const [authorTitle, setAuthorTitle] = useState("");
  const [articleTitle, setArticleTitle] = useState("");
  const [articleCategories, setArticleCategories] = useState("");
  const [articleImage, setArticleImage] = useState("");
  const [imageUrl, setImageUrl] = useState("");

  const hiddenFileInput = useRef(null);
  const { isLoading } = useSnapshot(states);

  let navigate = useNavigate();

  /* function getBase64(file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      console.log(reader.result);
    };
    reader.onerror = function (error) {
      console.log("Error: ", error);
    };
  } */

  /*   function toBase64(file) {
    const reader = new FileReader();
    reader.onloadend = () => {
      console.log(reader.result);
      // Logs data:<type>;base64,wL2dvYWwgbW9yZ...
    };
    reader.readAsDataURL(file);
  } */

  useEffect(() => {
    const auth = getAuth();

    onAuthStateChanged(auth, (user) => {
      if (user) {
        const db = getFirestore();
        const uid = user.uid;

        onSnapshot(collection(db, "users"), (snapshot) => {
          const users = snapshot.docs.map((doc) => ({
            ...doc.data(),
            id: doc.id,
          }));
          const currentUser = users.filter((user) => user.id === uid);

          setAuthor(currentUser[0].name);
          setAuthorTitle(currentUser[0].title);
        });
      } else {
      }
    });
  }, []);

  const storage = getStorage();
  const storageRef = ref(
    storage,
    //`${articleTitle.toLowerCase().split(" ").join("-")}`
    `images/${articleImage.name}`
  );

  /*  const storageFileRef = ref(
    storage,
    `articles/${articleTitle.toLowerCase().split(" ").join("-")}.md`
  ); */

  const data = {
    author: {
      name: author,
      title: authorTitle,
    },
    categories: articleCategories.split(","),
    date: new Date(Date.now()),
    //description: articleDescription,
    content: articleData,
    image: `gs://library-5f758.appspot.com/images/${articleImage.name}`,
    title: articleTitle,
  };

  return (
    <MainLayout>
      <div className="m-8 flex flex-col lg:flex-row">
        <form className="flex flex-col lg:w-1/2">
          <h1 className="text-2xl font-bold">Article Editor</h1>
          {/*   <div className="flex flex-col my-4">
            <label>Name of the author</label>
            <input
              value={author}
              onChange={(e) => setAuthor(e.target.value)}
              className="border-2 border-slate-600 p-3"
              type="text"
              placeholder="Enter author's name"
            />
          </div> */}
          {/*  <div className="flex flex-col my-4">
            <label>Title of the author</label>
            <input
              value={authorTitle}
              onChange={(e) => setAuthorTitle(e.target.value)}
              className="border-2 border-slate-600 p-3"
              type="text"
              placeholder="Enter author's title"
            />
          </div> */}
          <div className="flex flex-col my-4">
            <label>Title of the article</label>
            <input
              onChange={(e) => setArticleTitle(e.target.value)}
              className="border-2 border-slate-600 p-3"
              type="text"
              placeholder="Enter article's title"
            />
          </div>
          <div className="flex flex-col my-4">
            <label>Categories</label>
            <input
              onChange={(e) => setArticleCategories(e.target.value)}
              className="border-2 border-slate-600 p-3"
              type="text"
              placeholder={`Categories separated by commas. Ex: flutter, mobile, stacked`}
            />
          </div>
          <div className="flex flex-col my-4">
            <label>Image of the article</label>
            <Button
              color="secondary"
              className="flex flex-row w-full md:w-48 justify-center items-center gap-x-4"
              onClick={(e) => {
                e.preventDefault();
                hiddenFileInput.current.click();
              }}
            >
              <span>
                <FontAwesomeIcon icon={faUpload} />
              </span>
              <span>Upload Image</span>
            </Button>
            <input
              ref={hiddenFileInput}
              onChange={(e) => {
                setArticleImage(e.target.files[0]);
                setImageUrl(URL.createObjectURL(e.target.files[0]));
              }}
              className="border-2 hidden border-slate-600 p-3"
              type="file"
            />
          </div>
          {articleImage && (
            <div className="mb-4">
              <img
                className="w-24 h-24 object-cover"
                src={imageUrl}
                alt="Image preview"
              />
            </div>
          )}
          {/* <div className="flex flex-col my-4">
            <label>Description</label>
            <input
              onChange={(e) => setArticleDescription(e.target.value)}
              className="border-2 border-slate-600 p-3"
              type="text"
              placeholder={`Categories separated by commas. Ex: flutter, mobile, stacked`}
            />
          </div> */}
          <div className="flex flex-col">
            <label>Article</label>
            <textarea
              placeholder="Type your article here"
              className="p-3 border-2 h-96 border-slate-600"
              onChange={(e) => setArticleData(e.target.value)}
            ></textarea>
          </div>
          <Button
            disabled={isLoading}
            loading={isLoading}
            type="submit"
            className="my-4"
            onClick={async (e) => {
              e.preventDefault();
              actions.toggleLoader();

              console.log("test", articleImage.name);

              await addData("articles", data);
              // 'file' comes from the Blob or File API
              await uploadBytes(storageRef, articleImage).then((snapshot) => {
                console.log("Uploaded a blob or file!");
                console.log(snapshot);
              });
              /*  await uploadBytes(storageFileRef, articleData).then(
                (snapshot) => {
                  console.log("Uploaded a blob or file!");
                  console.log(snapshot);
                }
              ); */

              toast.success("Your article has been posted");
              actions.toggleLoader();

              setTimeout(() => {
                navigate("/");
              }, 2000);
            }}
          >
            Submit
          </Button>
        </form>
        <div className="flex flex-col lg:w-1/2">
          <h1 className="text-2xl m-4 font-bold">Preview in Markdown</h1>
          <div className="bg-green-50 my-2 mx-4 p-4 prose lg:prose-lg xl:prose-xl">
            {/* <ReactMarkdown children={articleData} remarkPlugins={[remarkGfm]} /> */}
            <MarkdownComponent file={articleData}></MarkdownComponent>
          </div>
        </div>
      </div>
    </MainLayout>
  );
};

export default EditorPage;
