import React from "react";

export const BlogLoader = () => {
  return (
    <div className="px-8 py-8 lg:px-8">
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-x-4 gap-y-12">
        {Array.from({ length: 6 }).map((_, index) => (
          <CardLoader key={index} />
        ))}
      </div>
    </div>
  );
};

const CardLoader = () => {
  return (
    <div className="flex flex-col">
      <div className="box h-52"></div>
      <div className="box h-2 mt-2 w-1/6"></div>
      <div className="box h-2 mt-2 w-full"></div>
      <div className="box h-2 mt-2 w-1/5"></div>
    </div>
  );
};
