import {
  collection,
  getFirestore,
  limit,
  orderBy,
  query,
} from "firebase/firestore";
import React, { useEffect, useState } from "react";
import { useCollection } from "react-firebase-hooks/firestore";
import toast from "react-hot-toast";
import { BlogCard } from "../../components/Card";
import { MainLayout } from "../../layouts/main";
import { firebaseApp } from "../../services/firebase.service";
import { BlogLoader } from "./Loader";
import { motion } from "framer-motion";
import useQuery from "../../hooks/useQuery";
import ArticlePage from "../Article";

const HomePage = () => {
  const [articles, setArticles] = useState([]);
  const [value, loading, error] = useCollection(
    query(
      collection(getFirestore(firebaseApp), "articles"),
      limit(10),
      orderBy("date", "desc")
    ),
    {
      snapshotListenOptions: { includeMetadataChanges: true },
    }
  );

  const params = useQuery();
  const article = params.get("article");

  useEffect(() => {
    if (error) {
      toast.error(error);
    }

    if (value) {
      setArticles(value.docs);
    }
  }, [loading]);

  return article ? (
    <ArticlePage article={article} />
  ) : (
    <MainLayout>
      <motion.div
        key="home.frame"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
      >
        {loading ? (
          <BlogLoader />
        ) : articles.length > 0 ? (
          <div className="p-8 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-x-4 gap-y-12">
            {articles.map((article) => {
              return (
                <BlogCard
                  key={article.id}
                  id={article.id}
                  {...article.data()}
                ></BlogCard>
              );
            })}
          </div>
        ) : (
          <div className="w-full h-80 flex justify-center items-center">
            Aucun article disponible
          </div>
        )}
      </motion.div>
    </MainLayout>
  );
};

export default HomePage;
