import React from "react";
import { Footer } from "../components/Footer";
import Navbar from "../components/Navbar";

export const MainLayout = ({ children }) => {
  return (
    <div>
      <Navbar />
      <main className="min-h-[75vh]">{children}</main>
      <Footer />
    </div>
  );
};
