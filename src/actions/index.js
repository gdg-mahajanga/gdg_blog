import { states } from "../states";

export const actions = {
  toggleLoader: () => {
    states.isLoading = !states.isLoading;
  },
};
