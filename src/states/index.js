import { proxy } from "valtio";

export const states = proxy({
  input: {},
  selectedLink: "home",
  isLoading: false,
  showMessage: false,
  user: localStorage.getItem("user"),
});
