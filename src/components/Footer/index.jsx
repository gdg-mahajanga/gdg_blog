import {
  faFacebookF,
  faInstagram,
  faLinkedin,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import { faEnvelope, faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

const links = [
  { icon: faFacebookF, link: "https://web.facebook.com/ailifidaaliotti" },
  { icon: faTwitter, link: "https://twitter.com/FidaAili" },
  {
    icon: faLinkedin,
    link: "https://www.linkedin.com/in/fida-aliotti-wylog-a%C3%AFli-2123b520b/",
  },
  { icon: faInstagram, link: "mailto:aili.fida.ac@gmail.com" },
  { icon: faEnvelope, link: "mailto:aili.fida.ac@gmail.com" },
];

export const Footer = () => {
  return (
    <div className="bg-gray-900 flex text-white justify-between items-center px-8 py-16">
      <h3>
        <FontAwesomeIcon
          className="text-red-600"
          icon={faHeart}
        ></FontAwesomeIcon>
        <span className="ml-1.5">from Madagascar</span>
      </h3>
      <ul className="flex w-1/2 md:w-1/4 justify-evenly">
        {links.map((link, index) => {
          return (
            <li key={index} className="text-xl">
              <a href={link.link}>
                <span>
                  <FontAwesomeIcon
                    color="white"
                    icon={link.icon}
                  ></FontAwesomeIcon>
                </span>
              </a>
            </li>
          );
        })}
      </ul>
    </div>
  );
};
