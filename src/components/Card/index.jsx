import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { getDownloadURL, getStorage, ref } from "firebase/storage";
import moment from "moment";
import React, { useEffect, useState } from "react";
import toast from "react-hot-toast";
import { Link } from "react-router-dom";
import { firebaseApp } from "../../services/firebase.service";
import { Button } from "../Buttons";

const Card = () => {
  return (
    <div className="border-2 p-4 shadow-md">
      <h1 className="text-2xl font-bold">Hello World</h1>
      <p className="text-sm my-4">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur nihil
        fuga repellat odit! Incidunt, corrupti. Ut quos ad ipsum itaque
        blanditiis non rerum! A, debitis fugiat suscipit nesciunt illum at.
      </p>
      <Button wide>Hello</Button>
    </div>
  );
};

const BlogCard = (props) => {
  const sanitizeDate = (date) => {
    return moment(date.toDate()).format("DD MMM YYYY");
  };

  const [image, setImage] = useState("");

  const imagePattern = /gs:\/\//;
  const testImagePath = imagePattern.test(props.image);

  useEffect(() => {
    setImage(props.image);

    if (testImagePath) {
      getDownloadURL(ref(getStorage(firebaseApp), props.image))
        .then(async (image) => {
          await setImage(image);
        })
        .catch((err) => toast.error(err.message));
    }
  }, []);

  return (
    <Link to={`/article/${props.id}`}>
      <div className="w-full shadow-md h-[350px]">
        <div>
          <span className="absolute text-xs bg-white px-2 py-1 rounded-md m-3 shadow-md">
            {props.categories.join(", ")}
          </span>
          <div
            className="h-52 w-full rounded-sm bg-cover bg-center"
            style={{
              backgroundImage: `url(${image})`,
            }}
          ></div>
        </div>
        <div className="p-4 border-t-2 border-gray-50">
          <div className="text-xs mt-4 text-gray-400">
            {sanitizeDate(props.date)}
          </div>
          <div className="font-medium text-lg">{props.title}</div>
          <div className="text-xs text-gray-400 font-light mt-1">
            <FontAwesomeIcon size="xs" icon={faUser}></FontAwesomeIcon>
            <span className="ml-2">{props.author.name}</span>
          </div>
        </div>
      </div>
    </Link>
  );
};

export { Card, BlogCard };
