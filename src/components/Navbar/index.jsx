import {
  faPencil,
  faSignIn,
  faSignOut,
  faSignOutAlt,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import toast from "react-hot-toast";
import { Link, useNavigate } from "react-router-dom";
import { useSnapshot } from "valtio";
import { states } from "../../states";
import { Button } from "../Buttons";

const Navbar = () => {
  const { user } = useSnapshot(states);
  let navigate = useNavigate();

  const logout = async () => {
    await localStorage.removeItem("user");
    await toast.success("Vous vous êtes déconnecté");
    setTimeout(() => {
      window.location.reload();
    }, 2000);
  };

  const writeArticle = () => {
    let screenToNavigate = "login";
    if (localStorage.getItem("user")) {
      screenToNavigate = "article";
    }

    navigate(screenToNavigate);
  };

  return (
    <nav className="py-8 flex px-8 items-center justify-between border-b-2">
      <Link to="/" className="font-black">
        {"> "}GDG MAHAJANGA
        <span className="animate-ping">__</span>
      </Link>
      <div className="md:hidden flex flex-row">
        <FontAwesomeIcon icon={faPencil} onClick={writeArticle} />

        {user && (
          <>
            <div className="mx-2"></div>
            <FontAwesomeIcon icon={faSignOut} onClick={logout} />
          </>
        )}
      </div>
      <div className="hidden md:block">
        <Button onClick={writeArticle}>
          <FontAwesomeIcon icon={faPencil} /> Ecrire un article
        </Button>
        {user ? (
          <Button color="secondary" className="mx-4" onClick={logout}>
            <FontAwesomeIcon icon={faSignOutAlt} /> Se déconnecter
          </Button>
        ) : (
          <Button className="mx-4" onClick={() => navigate("/login")}>
            <FontAwesomeIcon icon={faSignIn} /> Se connecter
          </Button>
        )}
      </div>
    </nav>
  );
};

export default Navbar;
