import React from "react";
import PropTypes from "prop-types";
import { states } from "../../states";

const Input = ({ wide, isRequired, ...props }) => {
  return (
    <input
      {...props}
      required={isRequired}
      onChange={(e) => {
        states.input[props.name] = e.target.value;
      }}
      className={`border-2 rounded-md px-2 py-2 ${props.className} ${
        props.bordercolor
      } ${wide && "w-full"}`}
    />
  );
};

Input.defaultProps = {
  type: "text",
  placeholder: "",
  bordercolor: "border-gray-500",
  name: "",
  wide: true,
  isRequired: true,
};

// Proptypes
Input.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string,
  bordercolor: PropTypes.string,
  name: PropTypes.string,
  onClick: PropTypes.func,
  wide: PropTypes.bool,
  isRequired: PropTypes.bool,
};

export default Input;

export const TextArea = (props) => {
  return (
    <textarea
      {...props}
      onChange={(e) => {
        states.input[props.name] = e.target.value;
      }}
      className={`border-2 rounded-md px-2 py-2 ${props.className} ${
        props.bordercolor
      } ${props.wide && "w-full"}`}
    ></textarea>
  );
};

TextArea.defaultProps = {
  placeholder: "",
  bordercolor: "border-gray-500",
  name: "",
};

TextArea.propTypes = {
  placeholder: PropTypes.string,
  bordercolor: PropTypes.string,
  name: PropTypes.string,
};
