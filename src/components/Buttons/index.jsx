import React from "react";
import PropTypes from "prop-types";
import Loader from "../Loader";

export const Button = (props) => {
  const setColor = () => {
    switch (props.color) {
      case "primary":
        return props.type === "text" ? "text-blue-600" : "bg-blue-600";
      case "secondary":
        return props.type === "text" ? "text-gray-600" : "bg-gray-600";
      case "accent":
        return props.type === "text" ? "text-green-600" : "bg-green-600";
      default:
        return props.type === "text" ? "text-blue-600" : "bg-blue-600";
    }
  };

  const setType = () => {
    switch (props.type) {
      case "default":
        break;
      case "outlined":
        break;
      default:
        break;
    }
  };

  return (
    <button
      className={`${props.wide && "w-full"} ${
        props.type !== "text" && "px-4 py-3"
      } ${setColor()} text-white rounded-md ${props.className}`}
      onClick={props.onClick}
      size={props.size}
    >
      {props.loading ? <Loader /> : props.children}
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.node,
  type: PropTypes.oneOf(["default", "outlined", "text"]),
  size: PropTypes.oneOf(["sm", "md", "lg"]),
  onClick: PropTypes.func,
  color: PropTypes.oneOf(["primary", "secondary", "accent"]),
  className: PropTypes.string,
  wide: PropTypes.bool,
  loading: PropTypes.bool,
};

Button.defaultProps = {
  children: "",
  size: "md",
  type: "default",
  onClick: null,
  color: "primary",
  className: "",
  wide: false,
  loading: false,
};
