import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { useSnapshot } from "valtio";
import AboutPage from "./pages/About";
import HomePage from "./pages/Home";
import LoginPage from "./pages/Login";
import RegisterPage from "./pages/Register";
import { states } from "./states";
import { AnimatePresence, motion } from "framer-motion";
import { Toaster } from "react-hot-toast";
import ArticlePage from "./pages/Article";
import EditorPage from "./pages/Editor";
import ResetPassword from "./pages/Login/ResetPassword";

export const App = () => {
  const { user } = useSnapshot(states);
  return (
    <Router>
      <AnimatePresence>
        <Toaster />
        <Routes>
          <Route path="/" element={<HomePage />}></Route>
          <Route path="/login" element={<LoginPage />}></Route>
          <Route path="/register" element={<RegisterPage />}></Route>
          <Route path="/about" element={<AboutPage />}></Route>
          <Route path="/article/:id" element={<ArticlePage />}></Route>
          <Route
            path="/article"
            element={user ? <EditorPage /> : <LoginPage />}
          ></Route>
          <Route path="/reset-password" element={<ResetPassword />}></Route>
        </Routes>
      </AnimatePresence>
    </Router>
  );
};
