import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyB5EUuPWJ9dSFw3GDdEFInTB2MBURsWawQ",
  authDomain: "library-5f758.firebaseapp.com",
  databaseURL: "https://library-5f758.firebaseio.com",
  projectId: "library-5f758",
  storageBucket: "library-5f758.appspot.com",
  messagingSenderId: "1453380321",
  appId: "1:1453380321:web:1164fd0f6be66f17865da0",
};

export const firebaseApp = initializeApp(firebaseConfig);
