import { firebaseApp } from "./firebase.service";

const fetchAll = (collection) => {
  return firebaseApp.firestore().collection(collection).get();
};
const fetchOne = (collection, id) => {
  return firebase.firestore().collection(collection).doc(id).get();
};

const fetchFileFromStorage = (url) => {
  return firebaseApp.storage().refFromURL(url);
};

export { fetchAll, fetchFileFromStorage, fetchOne };
