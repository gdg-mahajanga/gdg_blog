import { doc, getDoc, getFirestore, setDoc } from "firebase/firestore";
import { nanoid } from "nanoid";
import { actions } from "../actions";
import { firebaseApp } from "./firebase.service";

export const addData = async (collection, data, id = null) => {
  actions.toggleLoader();
  const _id = id || nanoid();
  const result = await setDoc(
    doc(getFirestore(firebaseApp), collection, _id),
    data
  );

  actions.toggleLoader();
};

export const setuserData = async (db) => {
  const user = JSON.parse(localStorage.getItem("user"));
  const userRef = doc(db, "users", user.uid);
  const userSnap = await getDoc(userRef);
  if (userSnap.exists()) {
    localStorage.setItem(
      "user",
      JSON.stringify({ ...userSnap.data(), uid: user.uid })
    );
  }
};
