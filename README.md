# GDG Blog

## Firebase CORS Problem

When downloading files (Markdown files), we are stumbled across CORS problem. To solve this, refer to [this link](https://firebase.google.com/docs/storage/web/download-files#cors_configuration)

- Create a json file named cors on root
- Copy and paste these instructions json`
  [
  {
  "origin": ["*"],
  "method": ["GET"],
  "maxAgeSeconds": 3600
  }
  ]

`

- Run `gsutil cors set cors.json gs://<your-cloud-storage-bucket>`

You must install [gsutil cli](https://cloud.google.com/storage/docs/gsutil_install) to take effect.

## GSUtil installation

After downloading the CLI, you need to run `gcloud init` to connect to your account

## DONE

- ✅ Ajout d'un créateur de contenu
- ✅ Proposer aux utilisateurs de s'inscrire si hors Messenger
- ✅ Ajout de la fonctionnalité de changement de mot de passe
- ✅ Possibilité de commenter l'article

## TO DO

- [ ] Ajout d'un création de contenu
- [ ] Possibilité de voir le blog sur Messenger et d'écrire du contenu
- [ ] Mise en cache des informations
- [ ] Possibilité de noter l'article
- [ ] Reset scroll on page change
